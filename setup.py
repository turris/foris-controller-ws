#!/usr/bin/env python

#
# foris-controller-ws
# Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from setuptools import setup
from foris_ws import __version__

DESCRIPTION = """
Implementation of websocket server for foris notification system.
"""

setup(
    name='foris-controller-ws',
    version=__version__,
    author='CZ.NIC, z.s.p.o. (http://www.nic.cz/)',
    author_email='stepan.henek@nic.cz',
    packages=[
        'foris_controller_ws',
    ],
    url='https://gitlab.labs.nic.cz/turris/foris-controller-ws',
    license='COPYING',
    description=DESCRIPTION,
    long_description=open('README.rst').read(),
    install_requires=[
        'websockets',
        'foris-client',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
        'websocket-client',
        'foris-controller-testtools',
    ],
    entry_points={
        "console_scripts": [
            "foris-controller-ws = foris_controller_ws.__main__:main",
        ]
    },
    dependency_links=[
        "git+https://gitlab.labs.nic.cz/turris/foris-controller-testtools.git#egg=foris-controller-testtools",
        "git+https://gitlab.labs.nic.cz/turris/foris-client.git#egg=foris-client",
    ],
    zip_safe=False,
)
