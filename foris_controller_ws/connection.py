#
# foris-controller-ws
# Copyright (C) 2018 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#


import asyncio
import json
import logging
import threading
import websockets
import uuid

from typing import List, Set, Union, Callable, Type

from functools import wraps
from collections import Iterable

logger = logging.getLogger(__name__)


def _with_lock(func: Callable) -> Callable:
    """Wraps function with self.lock

    :param func: the function to be wrapped
    :returns: wrapped function
    """

    @wraps(func)
    def inner(self, *args, **kwargs):
        with self.lock:
            return func(self, *args, **kwargs)

    return inner


class Connection:
    """ Class which represents the connection between the client and the websocket server
    """
    PING_THREAD_TIMEOUT: float = 60.0

    @staticmethod
    def generate_id() -> str:
        return str(uuid.uuid4())

    def __init__(self, client_id: int, handler: websockets.WebSocketServerProtocol):
        """ Initializes the connection

        :param client_id: unique client id
        :param handler: handler which is used to communicate with the client
        """
        self.client_id: int = client_id
        self.handler: websockets.WebSocketServerProtocol = handler
        self.lock: threading.Lock = threading.Lock()
        self.modules: Set[str] = set()
        self.exiting: bool = False

    @_with_lock
    async def send_message_to_client(self, msg: dict):
        """ Sends a message to the connected client
        :param msg: message to be sent to the client (in json format)
        :type msg: dict
        """
        str_msg = json.dumps(msg)
        logger.debug("Sending message to client %d: %s", self.client_id, str_msg)
        await self.handler.send(str_msg)

    async def process_message(self, message: str):
        """ Processes a message which is recieved from the client
        :param message: message which will be processed
        """
        try:
            parsed: dict = json.loads(message)

        except ValueError as e:
            logger.warning("The message is not in json format. (%s)" % message)
            logger.warning("Closing connection (id=%d)." % self.client_id)
            self.close()

        message_id = message.pop("id", self.generate_id())
        response = await loop.run_in_executor(None, )
        response = .... # TODO asycnion long connection

        response["id"] = message_id
        # send via foris client
        await self.send_message_to_client(response)

    def close(self):
        """ Sets a flag which should eventually close the connection.
        """
        self.exiting = True


class Connections:
    """ Class which represents all active connections
    """
    client_id: int = 1

    def __init__(self):
        """ Initializes Connections
        """
        self.lock = threading.Lock()
        self._connections = {}
        self.current_event_loop: Type[asyncio.AbstractEventLoop] = asyncio.get_event_loop()

    @_with_lock
    async def register_connection(self, handler: websockets.WebSocketServerProtocol):
        """ creates and adds a Connection instance among active connections

        :param handler: handler which is used to communicate with the client
        """
        new_client_id = Connections.client_id
        self._connections[new_client_id] = Connection(new_client_id, handler)
        Connections.client_id += 1
        return new_client_id

    @_with_lock
    def remove_connection(self, client_id: int):
        """ removes a Connection instance from active connections

        :param client_id: unique client id
        """
        if client_id not in self._connections:
            return
        try:
            self._connections[client_id].close()
        except Exception:
            pass
        del self._connections[client_id]

    @_with_lock
    async def handle_message(self, client_id: int, message: str):
        """ Handles a message recieved from the client

        :param client_id: unique client id
        :param message: message to be handeled
        """
        if client_id not in self._connections:
            logging.warning("Client '%d' is present it the connection list" % client_id)
            return
        try:
            await self._connections[client_id].process_message(message)
        except Exception as e:
            logging.error("Exception was raised: %s" % str(e))
            raise

    @_with_lock
    def publish_notification(self, module: str, message: dict):
        """ Publishes notification of the module to clients which have the module subscribed
            does nothing if no module is present in the message

        :param module: name of the module related to the notification
        :param message: a notification which will be published to all relevant clients
        """
        for _, connection in self._connections.items():
            message["id"] == self.generate_id()
            asyncio.run_coroutine_threadsafe(  # can be scheduled from another thread
                connection.send_message_to_client(message),
                self.current_event_loop,
            )


connections = Connections()
