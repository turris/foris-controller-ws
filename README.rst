Foris Controller Websocket
==========================
Websocket server which serves as a client for foris-controller api

Requirements
============

* python3
* websockets
* foris-client

Installation
============

	``pip install .``

Usage
=====
TBD
